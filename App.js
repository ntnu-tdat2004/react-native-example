// @flow
import React from 'react';
import {Component} from 'react-simplified';
import {StyleSheet, Button, Picker, Text, TextInput, View} from 'react-native';
import moji from 'moji-translate';

export default class App extends Component {
  translation = '';

  render() {
    // View components are similar to div elements
    return (
      <View style={styles.container}>
        <TextInput style={styles.textInput} placeholder="Translate" onChangeText={this.translate} />
        <Text style={styles.text}>{this.translation} </Text>
      </View>
    );
  }

  translate(text: string) {
    this.translation = moji.translate(text);
  }
}

// Instead of CSS, a style sheet designed for native apps is used:
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: 'center'
  },
  textInput: {
    fontSize: 20,
    height: 40,
    borderWidth: 1
  },
  text: {
    fontSize: 42
  }
});
